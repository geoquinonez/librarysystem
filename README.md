# Library System

Implement a REST API in Java/Kotlin with data persistency to provide the following functionality:

### Introduction:
A User wants a small system to store all the books he owns, authors, genres and **read state**

### Feature requirements:

- User should be able to add, get, delete authors
- User should be able to add, get, delete genres
- User should be able to add a book with it's author/s, genre and **read state**
- User can update **read state**
- User should be able to delete any book
- User should be able to get books by author or by genre

> Read state could be [UNREAD, READING, ALREADY READ]

### Bonus requirements:

- Unit Testing is a plus
- Swagger will be very appreciated


### Project requirements and tools:

- Spring boot should be the core of this challenge
- Implement data persistency on a SQL Database, could be SQLite, Postgres or MariaDB. Preferably MariaBD
- Feel free to use any extra tool, library or dependecy that helps you or your reviewer, like Docker, Kubernetes, or any Java library to facilitate the development process
- You should use a git VCS like github, gitlab or bitbucket
- You can use any library if you want to do Unit Testing. Preferably Junit and/or Mockito
- Please provide some documentation in a README file (preferably markdown) explaining how to run application, unit tests (if exists), migrations, endpoints, etc
