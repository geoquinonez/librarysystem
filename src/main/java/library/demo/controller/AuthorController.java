package library.demo.controller;


import library.demo.dto.AuthorDTO;
import library.demo.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "api/library/author")
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    @GetMapping
    public List<AuthorDTO> getAuthors() {
        return authorService.getAuthors().stream().map(AuthorDTO::new).collect(Collectors.toList());

    }

    @PostMapping
    public void registerNewAuthor(@RequestBody AuthorDTO authorDTO) {
        authorService.addNewAuthor(AuthorDTO.toEntity(authorDTO));
    }

    @DeleteMapping(path = "{authorId}")
    public void deleteAuthor(@PathVariable("authorId") Long authorId) {
        authorService.deleteAuthor(authorId);
    }

}
