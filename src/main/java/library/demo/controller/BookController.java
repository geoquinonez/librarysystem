package library.demo.controller;

import library.demo.dto.BookDTO;
import library.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "api/library/book")
public class BookController {

    @Autowired
    private BookService bookService;

    @PostMapping
    public void registerNewBookByGenreId(@RequestBody BookDTO bookDTO, @RequestParam("genreId") Long genreId){
        bookService.addNewBookByGenreIdAuthors(bookDTO,genreId);
    }

    @GetMapping
    public List<BookDTO> getBooks() {
         return bookService.getBooks().stream().map(BookDTO::new).collect(Collectors.toList());
    }


}
