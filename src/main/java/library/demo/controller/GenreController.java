package library.demo.controller;

import library.demo.dto.GenreDTO;
import library.demo.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "api/library/genre")
public class GenreController {

    @Autowired
    private GenreService genreService;

    @GetMapping
    public List<GenreDTO> getGenres(){
        return genreService.getGenres().stream().map(GenreDTO::new).collect(Collectors.toList());
    }

    @PostMapping
    public void registerNewGenre(@RequestBody GenreDTO genreDTO){
        genreService.addNewGenre(GenreDTO.toEntity(genreDTO));
    }

    @DeleteMapping(path = "{genreId}")
    public void deleteGenre(@PathVariable("genreId") Long  genreId){
        genreService.deleteGenre(genreId);

    }



}
