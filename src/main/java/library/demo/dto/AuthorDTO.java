package library.demo.dto;

import library.demo.entity.AuthorEntity;

public class AuthorDTO {

    private Long authorId;
    private String firstName;
    private String lastName;

    public AuthorDTO(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public AuthorDTO() {
    }

    public AuthorDTO(AuthorEntity authorEntity) {
        this.authorId = authorEntity.getAuthorId();
        this.firstName = authorEntity.getFirstName();
        this.lastName = authorEntity.getLastName();

    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public static AuthorEntity toEntity(AuthorDTO authorDTO) {
        AuthorEntity authorEntity = new AuthorEntity(authorDTO.getAuthorId(),authorDTO.getFirstName(),authorDTO.getLastName());
        return authorEntity;
    }

}
