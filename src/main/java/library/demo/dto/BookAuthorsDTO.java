package library.demo.dto;

public class BookAuthorsDTO {

    private Long bookAuthorsId;
    private Long bookEntity;
    private Long authorEntity;

    public Long getBookAuthorsId() {
        return bookAuthorsId;
    }

    public void setBookAuthorsId(Long bookAuthorsId) {
        this.bookAuthorsId = bookAuthorsId;
    }

    public Long getBookEntity() {
        return bookEntity;
    }

    public void setBookEntity(Long bookEntity) {
        this.bookEntity = bookEntity;
    }

    public Long getAuthorEntity() {
        return authorEntity;
    }

    public void setAuthorEntity(Long authorEntity) {
        this.authorEntity = authorEntity;
    }

    @Override
    public String toString() {
        return "BookAuthorsDTO{" +
                "bookAuthorsId=" + bookAuthorsId +
                ", bookEntity=" + bookEntity +
                ", authorEntity=" + authorEntity +
                '}';
    }
}
