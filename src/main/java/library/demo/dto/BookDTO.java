package library.demo.dto;

import library.demo.entity.BookEntity;

import java.util.Set;

public class BookDTO {


    private Long bookId;
    private String isbn;
    private String title;
    private String description;
    private String readStatus;
    private Long genreId;
    private Set<Long> authors;

    public BookDTO() {
    }

    public BookDTO(BookEntity bookEntity){
        this.bookId = bookEntity.getBookId();
        this.isbn = bookEntity.getIsbn();
        this.title = bookEntity.getTitle();
        this.description = bookEntity.getDescription();
        this.readStatus = bookEntity.getReadStatus();

    }


    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus;
    }

    public Long getGenreEntity() {
        return genreId;
    }

    public void setGenreEntity(Long genreId) {
        this.genreId = genreId;
    }


    public Set<Long> getAuthorsDTO() {
        return authors;
    }

    public void setAuthors(Set<Long> authors) {
        this.authors = authors;
    }


    @Override
    public String toString() {
        return "BookDTO{" +
                "book_id=" + bookId +
                ", isbn='" + isbn + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", read_status='" + readStatus + '\'' +
                '}';
    }

    public static BookEntity ToEntity(BookDTO bookDTO){
        BookEntity bookEntity = new BookEntity(bookDTO.getIsbn(),bookDTO.getTitle(),
                bookDTO.getDescription(), bookDTO.getReadStatus(),bookDTO.getGenreEntity());
        return bookEntity;

    }

}
