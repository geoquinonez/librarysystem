package library.demo.dto;

import library.demo.entity.GenreEntity;

public class GenreDTO {

    private Long genreId;
    private String description;

    public GenreDTO(Long genreId, String description) {
        this.genreId = genreId;
        this.description = description;
    }

    public GenreDTO() {
    }

    public GenreDTO(GenreEntity genreEntity) {
        this.genreId = genreEntity.getGenreId();
        this.description = genreEntity.getDescription();
    }

    public Long getGenreId() {
        return genreId;
    }

    public void setGenreId(Long genreId) {
        this.genreId = genreId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static GenreEntity toEntity(GenreDTO genreDTO) {
        GenreEntity genreEntity = new GenreEntity(genreDTO.getGenreId(), genreDTO.getDescription());
        return genreEntity;
    }

}
