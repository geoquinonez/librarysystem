package library.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "Author")
@Table(name="author")
public class AuthorEntity {

    @Id
    @SequenceGenerator(
            name="author_sequence",
            sequenceName = "author_sequence",
            allocationSize = 1

    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "author_sequence"
    )
    @Column(
            name = "author_id",
            updatable = false
    )
    private Long authorId;

    @Column(
            name="first_name",
            columnDefinition = "TEXT",
            length = 100
    )
    private String firstName;

    @Column(
            name="last_name",
            columnDefinition = "TEXT",
            length = 100
    )
    private String lastName;

    public AuthorEntity(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public AuthorEntity(Long authorId, String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.authorId = authorId;
    }

    public AuthorEntity() {

    }


    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Author{" +
                "author_id=" + authorId +
                ", first_name='" + firstName + '\'' +
                ", last_name='" + lastName + '\'' +
                '}';
    }
}
