package library.demo.entity;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import static javax.persistence.GenerationType.SEQUENCE;
@Entity(name = "book_authors")
@Table(name = "book_authors")
public class BookAuthorsEntity {

    @Id
    @SequenceGenerator(
            name="book_authors_sequence",
            sequenceName = "book_authors_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy= SEQUENCE,
            generator = "book_authors_sequence"
    )
    @Column(
            name = "book_authors_id",
            updatable = false
    )
    private Long bookAuthorsId;

    @ManyToOne(
            optional = false,
            cascade = CascadeType.ALL

    )
    @JoinColumn(
            name = "book_id",
            referencedColumnName = "book_id"
    )
    private BookEntity bookEntity;


    @ManyToOne(
            optional = false,
            cascade = CascadeType.ALL

    )
    @JoinColumn(
            name = "author_id",
            referencedColumnName = "author_id"
    )
    private AuthorEntity authorEntity;

    public BookEntity getBookEntity() {
        return bookEntity;
    }

    public void setBookEntity(BookEntity bookEntity) {
        this.bookEntity = bookEntity;
    }

    public AuthorEntity getAuthorEntity() {
        return authorEntity;
    }

    public void setAuthorEntity(AuthorEntity authorEntity) {
        this.authorEntity = authorEntity;
    }

    @Override
    public String toString() {
        return "BookAuthorsEntity{" +
                "bookAuthorsId=" + bookAuthorsId +
                ", bookEntity=" + bookEntity +
                ", authorEntity=" + authorEntity +
                '}';
    }
}
