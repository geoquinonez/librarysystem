package library.demo.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "Book")
@Table(name="book")
public class BookEntity {

    @Id
    @SequenceGenerator(
            name="book_sequence",
            sequenceName = "book_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "book_sequence"
    )
    @Column(
            name = "book_id",
            updatable = false
    )
    private Long bookId;
    @Column(
            name = "isbn",
            columnDefinition = "TEXT",
            length = 13
    )
    private String isbn;
    @Column(
            name = "title",
            nullable = false,
            columnDefinition = "TEXT",
            length = 250
    )
    private String title;
    @Column(
            name = "description",
            columnDefinition = "TEXT",
            length = 350
    )
    private String description;
    @Column(
            name = "read_status",
            columnDefinition = "TEXT",
            length = 15
    )
    private String readStatus;

    @OneToOne(
            optional = false,
            cascade = CascadeType.ALL
    )
    @JoinColumn(
            name="genre_id",
            referencedColumnName = "genre_id"
    )
    private GenreEntity genreEntity;

    public BookEntity() {
    }

    public BookEntity(String isbn, String title, String description, String readStatus, Long genreEntity) {
        this.isbn = isbn;
        this.title = title;
        this.description = description;
        this.readStatus = readStatus;
        this.genreEntity = new GenreEntity(genreEntity);
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus;
    }

    public GenreEntity getGenreEntity() {
        return genreEntity;
    }

    public void setGenreEntity(GenreEntity genreEntity) {
        this.genreEntity = genreEntity;
    }

    @Override
    public String toString() {
        return "Book{" +
                "book_id=" + bookId +
                ", isbn='" + isbn + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", read_status='" + readStatus + '\'' +
                '}';
    }
}
