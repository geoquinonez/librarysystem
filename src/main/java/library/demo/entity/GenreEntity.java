package library.demo.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name="Genre")
@Table(name="genre")
public class GenreEntity {
    @Id
    @SequenceGenerator(
            name="genre_sequence",
            sequenceName = "genre_sequence",
            allocationSize = 1

    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "genre_sequence"
    )
    @Column(
            name = "genre_id",
            updatable = false
    )
    private Long genreId;
    @Column(
            name = "description",
            columnDefinition = "TEXT",
            length = 200
    )
    private String description;

    public GenreEntity(Long genreId, String description) {
        this.genreId = genreId;
        this.description = description;
    }

    public GenreEntity(String desription) {
        this.description = desription;
    }

    public GenreEntity(Long genreId) {
        this.genreId = genreId;
    }

    public GenreEntity() {
    }

    public Long getGenreId() {
        return genreId;
    }

    public void setGenreId(Long genreId) {
        this.genreId = genreId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "genre_id=" + genreId +
                ", desription='" + description + '\'' +
                '}';
    }
}
