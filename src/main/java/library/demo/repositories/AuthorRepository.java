package library.demo.repositories;

import library.demo.entity.AuthorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthorRepository extends JpaRepository<AuthorEntity,Long> {

    @Query(value = "SELECT a.* FROM author a WHERE first_name=:firstName and last_name=:lastName", nativeQuery = true)
    Optional<AuthorEntity> findByFullName(String firstName, String lastName);

}
