package library.demo.repositories;

import library.demo.entity.BookAuthorsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookAuthorsRepository extends JpaRepository<BookAuthorsEntity,Long> {
}
