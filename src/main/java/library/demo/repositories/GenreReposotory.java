package library.demo.repositories;

import library.demo.entity.GenreEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GenreReposotory extends JpaRepository<GenreEntity,Long> {
    Optional<GenreEntity> findGenreByDescription(String description);
}
