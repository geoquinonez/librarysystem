package library.demo.service;

import library.demo.entity.AuthorEntity;
import library.demo.repositories.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorService implements IAuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    @GetMapping
    @Override
    public List<AuthorEntity> getAuthors() {
        return authorRepository.findAll();
    }

    @PostMapping
    @Override
    public void addNewAuthor(AuthorEntity author) {
        Optional<AuthorEntity> findAuthor = authorRepository.findByFullName(author.getFirstName(), author.getLastName());
        if (findAuthor.isPresent()) {
            throw new IllegalStateException("AUTHOR EXIST");
        }
        authorRepository.save(author);
    }

    @Override
    public void deleteAuthor(Long authorId) {
        boolean author = authorRepository.existsById(authorId);
        if (!author) {
            throw new IllegalStateException(
                    "author ID : " + authorId + " does not exist");
        }
        authorRepository.deleteById(authorId);
    }
}
