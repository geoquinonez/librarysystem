package library.demo.service;

import library.demo.entity.AuthorEntity;
import library.demo.entity.BookAuthorsEntity;
import library.demo.entity.BookEntity;
import library.demo.repositories.AuthorRepository;
import library.demo.repositories.BookAuthorsRepository;
import library.demo.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

@Service
public class BookAuthorsService implements IBookAuthorsService {

    @Autowired
    private BookAuthorsRepository bookAuthorsRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<BookAuthorsEntity> getBookAuthors() {
        return bookAuthorsRepository.findAll();
    }

    @Override
    public void addNewBookAuthors(Long bookId, Long authorId) {
        Optional<BookEntity> bookfind = bookRepository.findById(bookId);
        if(!bookfind.isPresent()){
            throw new IllegalStateException("LIBRO NO EXISTE");
        }
        Optional<AuthorEntity> authorEntity = authorRepository.findById(authorId);
        if (!authorEntity.isPresent()){
            throw new IllegalStateException("AUTHOR NO EXISTE");
        }

    }
}
