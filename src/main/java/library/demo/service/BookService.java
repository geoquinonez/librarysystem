package library.demo.service;

import library.demo.dto.BookDTO;
import library.demo.entity.AuthorEntity;
import library.demo.entity.BookAuthorsEntity;
import library.demo.entity.BookEntity;
import library.demo.entity.GenreEntity;
import library.demo.repositories.AuthorRepository;
import library.demo.repositories.BookAuthorsRepository;
import library.demo.repositories.BookRepository;
import library.demo.repositories.GenreReposotory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BookService implements IBookService{

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private GenreReposotory genreReposotory;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private BookAuthorsRepository bookAuthorsRepository;

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public List<BookEntity> getBooks(){
        return bookRepository.findAll();
    }

    @Override
    public void addNewBookByGenreId(BookEntity bookEntity, Long genreId) {

        Optional<GenreEntity> findGenre = genreReposotory.findById(genreId);
        if(!findGenre.isPresent()){
            throw new IllegalStateException("GENRE NO EXIST");
        }
        bookEntity.setGenreEntity(findGenre.get());
        Optional<BookEntity> findBook = bookRepository.findByTitle(bookEntity.getTitle());
        if(findBook.isPresent()){
            throw  new IllegalStateException("BOOK EXIST");
        }
        bookRepository.save(bookEntity);
    }

    @Override
    public void addNewBookByGenreIdAuthors(BookDTO bookDTO, Long genreId) {
        try{
            Optional<GenreEntity> findGenre = genreReposotory.findById(genreId);
            if(!findGenre.isPresent()){
                throw new IllegalStateException("GENRE NOT EXIST");
            }
            Optional<BookEntity> findBook = bookRepository.findByTitle(bookDTO.getTitle());
            if(findBook.isPresent()){
                throw  new IllegalStateException("BOOK EXIST");
            }
            bookDTO.setGenreEntity(genreId);
           bookRepository.save(BookDTO.ToEntity(bookDTO));
            for(Long authorId : bookDTO.getAuthorsDTO()){
                BookAuthorsEntity bookAuthorsEntity  = new BookAuthorsEntity();
                Optional<AuthorEntity> findAuthor = authorRepository.findById(authorId);
                if(!findAuthor.isPresent()){
                    throw new IllegalStateException("AUTHOR NOT EXIST");
                }
                bookAuthorsEntity.setBookEntity(BookDTO.ToEntity(bookDTO));
                bookAuthorsRepository.save(bookAuthorsEntity);
            }
        } catch (Exception exception) {
            throw new IllegalStateException(exception.getMessage());
        }
    }

}
