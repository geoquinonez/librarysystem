package library.demo.service;

import library.demo.entity.GenreEntity;
import library.demo.repositories.GenreReposotory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GenreService implements IGenreService {

    @Autowired
    private GenreReposotory genreReposotory;

    public List<GenreEntity> getGenres() {
        return genreReposotory.findAll();
    }

    public void addNewGenre(GenreEntity genre) {
        Optional<GenreEntity> findGenre = genreReposotory.findGenreByDescription(genre.getDescription());
        if (findGenre.isPresent()) {
            throw new IllegalStateException("GENRE EXIST");
        }
        genreReposotory.save(genre);
    }


    public void deleteGenre(Long genreId) {
        boolean genre = genreReposotory.existsById(genreId);
        if (!genre) {
            throw new IllegalStateException(
                    "Genre ID : " + genreId + " does not exist ");
        }
        genreReposotory.deleteById(genreId);
    }
}
