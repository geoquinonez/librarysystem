package library.demo.service;

import library.demo.entity.AuthorEntity;

import java.util.List;

public interface IAuthorService {

    List<AuthorEntity> getAuthors();

    void addNewAuthor(AuthorEntity author);

    void deleteAuthor(Long authorId);
}
