package library.demo.service;

import library.demo.entity.BookAuthorsEntity;

import java.util.List;

public interface IBookAuthorsService {

     List<BookAuthorsEntity> getBookAuthors();

     void addNewBookAuthors(Long bookId, Long authorId);

}
