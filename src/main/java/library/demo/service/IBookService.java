package library.demo.service;

import library.demo.dto.BookDTO;
import library.demo.entity.BookEntity;

import java.util.List;

public interface IBookService  {

    List<BookEntity> getBooks();

    void addNewBookByGenreId(BookEntity bookEntity, Long genreId);

    void addNewBookByGenreIdAuthors(BookDTO bookDTO, Long genreId);

}
