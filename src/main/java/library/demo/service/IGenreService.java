package library.demo.service;

import library.demo.entity.GenreEntity;

import java.util.List;

public interface IGenreService {

    List<GenreEntity> getGenres();

    void addNewGenre(GenreEntity genre);

    void deleteGenre(Long genreId);

}
