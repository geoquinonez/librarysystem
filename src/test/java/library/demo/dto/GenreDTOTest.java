package library.demo.dto;

import library.demo.entity.GenreEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GenreDTOTest {

    private GenreDTO genreDTO1 = null;


    @BeforeEach
    public void setup(){
        genreDTO1 = new GenreDTO(1L,"TERROR");

    }


    @Test
    public void genreShouldHaveDescription(){
        GenreDTO genreDTO = new GenreDTO(1L,"FELIXANIO");
        Assertions.assertEquals("TERROR",genreDTO1.getDescription());
    }

    @Test
    public void shouldMapToEntity(){
        GenreEntity genreEntity = new GenreEntity(1L,"AMORFO");
        GenreDTO genreDTO = new GenreDTO(1L,"AMORFO");

        Assertions.assertEquals(genreEntity.getDescription(),GenreDTO.toEntity(genreDTO).getDescription());

    }



}
